package io.gitlab.croclabs.inject;

import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

public class AppModule extends AbstractModule {
	private final Map<Class<?>, List<Class<?>>> byInterface = new HashMap<>();

	@Override
	public void configure() {
		try {
			Stream<Class<?>> allClasses = ClassPath.from(ClassLoader.getSystemClassLoader())
					.getAllClasses()
					.stream()
					.filter(c -> !c.getName().contains("package-info") &&
							!c.getName().contains("module-info"))
					.map(ClassInfo::load);

			allClasses
					.filter(c -> c.isAnnotationPresent(IntoSet.class))
					.forEach(this::intoSet);

			bindByInterface();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void intoSet(Class<?> clazz) {
		Arrays.stream(clazz.getInterfaces()).forEach(i -> intoSetByInterface(i, clazz));
	}

	private void intoSetByInterface(Class<?> interfaceClass, Class<?> clazz) {
		byInterface.computeIfAbsent(interfaceClass, c -> new ArrayList<>());
		byInterface.get(interfaceClass).add(clazz);
	}

	private void bindByInterface() {
		byInterface.forEach((i, classes) -> {
			Multibinder<?> multibinder = Multibinder.newSetBinder(binder(), i);
			classes.forEach(c -> multibinder.addBinding().to(castClass(c)));
		});
	}

	@SuppressWarnings("unchecked")
	private <T> Class<T> castClass(Class<?> c) {
		return (Class<T>) c;
	}
}
