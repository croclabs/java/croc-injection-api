package io.gitlab.croclabs.inject;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class AppContext {
	public static final Injector app = Guice.createInjector(new AppModule());
}
